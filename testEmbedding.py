from scripts.vecEmbedding.word2vec_emb_kor import embedding
import numpy as np
import pandas as pd


md = embedding('korTrained_v2.bin')
# print(md.findSim('삼성전자', topN=10))
# print(md.findSim('호텔신라', topN=10))
# print(md.findSim('SK하이닉스', topN=10))
# print(md.findSim('만도', topN=10))

theme = ['에너지', '관광', '금융']
words = ['석유', '원자력', '중국', '호텔', '현금', '주식', '금리', '관광객', '발전소', '버스', '비행기']
for themeWord in theme:
    sims = [md.wordSim(themeWord, word) for word in words]
    sorted = np.argsort(sims)[::-1]
    print('=============================' + themeWord + '=============================')
    col1 = []
    col2 = []
    for index in sorted:
        col1.append(words[index])
        col2.append(sims[index])
    df = pd.DataFrame.from_dict({'Word': col1, 'Similarity': col2})
    print(df.to_string(index=False))
