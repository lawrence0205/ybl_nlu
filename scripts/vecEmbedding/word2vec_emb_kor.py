from gensim.models import FastText
from scripts.langTools.korDAssembler import *
import numpy as np
import os
"""
Lists of Methods:
wordEmb: 단어를 벡터로 embedding
wordSim: 두 개 단어 유사도 비교
findSim: 단어의 주변어 탐색
"""

class embedding:
    def __init__(self):
        self.root = './'
        print('Loading model...')
        self.model = FastText.load(os.path.join(self.root, 'trained/wordEmbedding/korTrained.bin'))
        print('Model loaded')
        self.dim = len(self.model.wv['a'])

    def wordEmb(self, word):
        """
        Vector embedding
        :param word: either string or list of string to be embedded
        :return: embedded string
        """
        if isinstance(word, str):
            return self.model.wv[decompose(word)]
        elif isinstance(word, list):
            return [self.model.wv[decompose(item)] for item in word]
        else:
            raise Exception('Only string or list of strings input available.')

    def wordSim(self, word1: str, word2: str):
        """
        Word similarity measure
        :param word1: word to be compared 1
        :param word2: word to be compared 2
        :return: word pair similarity
        """
        return self.model.wv.similarity(decompose(word1), decompose(word2))

    def findSim(self, word, topN=5, sim=True):
        """
        Finds top N many similar words
        :param word: string, vector
        :param topN: number of similar words to be retrieved
        :param sim: Boolean for displaying similarity
        :return: similar words
        """
        if isinstance(word, str):
            res = self.model.wv.similar_by_word(decompose(word), topn=topN)
            if sim:
                return [(assemble(item[0]), item[1]) for item in res]
            else:
                return [assemble(item[0]) for item in res]
        elif isinstance(word, np.ndarray):
            res = self.model.wv.similar_by_vector(word, topn=topN)
            if sim:
                return [(assemble(item[0]), item[1]) for item in res]
            else:
                return [assemble(item[0]) for item in res]
        else:
            raise Exception('Only string or ndarray vector input available.')

    def sen2matrix(self, sentence, maxDim=10):
        output = []
        sentence = sentence.split(' ')

