from gensim.models import FastText
import scripts.dataProcessors.dataLoader as dataLoader
import os

root = os.path.dirname(os.getcwd())
# Initialize
model = FastText(min_count=3, word_ngrams=1, min_n=2, max_n=7, size=100)

for idx, dat in enumerate(['wiki', 'naver']):
    print('Iteration count: ', idx + 1)
    update = False
    if idx > 0:
        update = True
    trainingData = dataLoader.loadData(dat)
    print('Training Data Loaded: ', dat)
    model.build_vocab(trainingData, update=update)
    print('Vocab built')
    model.train(trainingData, total_examples=len(trainingData), epochs=model.epochs)
    print(dat + ' Training Complete')

print('Training complete now saving...')
model.save(os.path.join(root, 'trained/korTrained.bin'))
print('Saved. Closing the session.')
