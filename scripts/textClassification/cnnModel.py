from __future__ import absolute_import, division, print_function
from tensorflow import keras


class TextConvNet(object):
    def objNet1(self, num_classes=100, wordVec_dim=100, max_sentence_length=10, conv_channel=128, l2_reg_lambda=0.01):
        # TODO 추후 input dimension을 하나 더 늘려서 non-static channel도 포함 시킬 것
        main_input = keras.layers.Input(shape=(max_sentence_length, wordVec_dim, 1), dtype='float', name='main_input')

        conv1 = keras.layers.Conv2D(conv_channel, kernel_size=(1, wordVec_dim), strides=(1, 1), activation='relu', padding='valid',
                                    input_shape=(max_sentence_length, wordVec_dim))(main_input)
        conv2 = keras.layers.Conv2D(conv_channel, kernel_size=(2, wordVec_dim), strides=(1, 1), activation='relu', padding='valid',
                                    input_shape=(max_sentence_length, wordVec_dim))(main_input)
        conv3 = keras.layers.Conv2D(conv_channel, kernel_size=(3, wordVec_dim), strides=(1, 1), activation='relu', padding='valid',
                                    input_shape=(max_sentence_length, wordVec_dim))(main_input)
        conv4 = keras.layers.Conv2D(conv_channel, kernel_size=(4, wordVec_dim), strides=(1, 1), activation='relu', padding='valid',
                                    input_shape=(max_sentence_length, wordVec_dim))(main_input)
        conv5 = keras.layers.Conv2D(conv_channel, kernel_size=(5, wordVec_dim), strides=(1, 1), activation='relu', padding='valid',
                                    input_shape=(max_sentence_length, wordVec_dim))(main_input)
        conv6 = keras.layers.Conv2D(conv_channel, kernel_size=(6, wordVec_dim), strides=(1, 1), activation='relu', padding='valid',
                                    input_shape=(max_sentence_length, wordVec_dim))(main_input)
        conv7 = keras.layers.Conv2D(conv_channel, kernel_size=(7, wordVec_dim), strides=(1, 1), activation='relu', padding='valid',
                                    input_shape=(max_sentence_length, wordVec_dim))(main_input)
        conv8 = keras.layers.Conv2D(conv_channel, kernel_size=(8, wordVec_dim), strides=(1, 1), activation='relu', padding='valid',
                                    input_shape=(max_sentence_length, wordVec_dim))(main_input)
        conv9 = keras.layers.Conv2D(conv_channel, kernel_size=(9, wordVec_dim), strides=(1, 1), activation='relu', padding='valid',
                                    input_shape=(max_sentence_length, wordVec_dim))(main_input)
        conv10 = keras.layers.Conv2D(conv_channel, kernel_size=(10, wordVec_dim), strides=(1, 1), activation='relu', padding='valid',
                                    input_shape=(max_sentence_length, wordVec_dim))(main_input)
        pooled1 = keras.layers.MaxPool2D((conv1.shape[1], 1), strides=(1, 1), padding='valid')(conv1)
        pooled2 = keras.layers.MaxPool2D((conv2.shape[1], 1), strides=(1, 1), padding='valid')(conv2)
        pooled3 = keras.layers.MaxPool2D((conv3.shape[1], 1), strides=(1, 1), padding='valid')(conv3)
        pooled4 = keras.layers.MaxPool2D((conv4.shape[1], 1), strides=(1, 1), padding='valid')(conv4)
        pooled5 = keras.layers.MaxPool2D((conv5.shape[1], 1), strides=(1, 1), padding='valid')(conv5)
        pooled6 = keras.layers.MaxPool2D((conv6.shape[1], 1), strides=(1, 1), padding='valid')(conv6)
        pooled7 = keras.layers.MaxPool2D((conv7.shape[1], 1), strides=(1, 1), padding='valid')(conv7)
        pooled8 = keras.layers.MaxPool2D((conv8.shape[1], 1), strides=(1, 1), padding='valid')(conv8)
        pooled9 = keras.layers.MaxPool2D((conv9.shape[1], 1), strides=(1, 1), padding='valid')(conv9)
        pooled10 = keras.layers.MaxPool2D((conv10.shape[1], 1), strides=(1, 1), padding='valid')(conv10)

        concat = keras.layers.concatenate([pooled1, pooled2, pooled3, pooled4, pooled5,
                                      pooled6, pooled7, pooled8, pooled9, pooled10])
        flatten = keras.layers.Flatten()(concat)
        dense = keras.layers.Dense(conv_channel*10,
                                   activation='relu',
                                   use_bias=True,
                                   kernel_regularizer=keras.regularizers.l2(l2_reg_lambda))(flatten)
        dropout = keras.layers.Dropout(0.5)(dense)
        output = keras.layers.Dense(num_classes, activation='softmax')(dropout)

        model = keras.models.Model(inputs=main_input, outputs=output)
        model.compile(optimizer=keras.optimizers.SGD(learning_rate=0.01, momentum=0.0, nesterov=False),
                      loss='sparse_categorical_crossentropy')
        model.summary()
        keras.utils.plot_model(model, to_file='./model.png')
        return model

        #
        # model.fit({'main_input': headline_data, 'aux_input': additional_data},
        #           {'main_output': labels, 'aux_output': labels},
        #           epochs=50, batch_size=32)



if __name__ == '__main__':
    model = TextConvNet()
