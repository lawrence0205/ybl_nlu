import re


# 유니코드 한글 시작 : 44032, 끝 : 55199
BASE_CODE, CHOSUNG, JUNGSUNG = 44032, 588, 28
# 초성 리스트. 00 ~ 18
CHOSUNG_LIST = ['ㄱ', 'ㄲ', 'ㄴ', 'ㄷ', 'ㄸ', 'ㄹ', 'ㅁ', 'ㅂ', 'ㅃ', 'ㅅ', 'ㅆ', 'ㅇ', 'ㅈ', 'ㅉ', 'ㅊ', 'ㅋ', 'ㅌ', 'ㅍ', 'ㅎ']
# 중성 리스트. 00 ~ 20
JUNGSUNG_LIST = ['ㅏ', 'ㅐ', 'ㅑ', 'ㅒ', 'ㅓ', 'ㅔ', 'ㅕ', 'ㅖ', 'ㅗ', 'ㅘ', 'ㅙ', 'ㅚ', 'ㅛ', 'ㅜ', 'ㅝ', 'ㅞ', 'ㅟ', 'ㅠ', 'ㅡ', 'ㅢ', 'ㅣ']
# 종성 리스트. 00 ~ 27 + 1(없는 경우 _로 표기)
JONGSUNG_LIST = ['_', 'ㄱ', 'ㄲ', 'ㄳ', 'ㄴ', 'ㄵ', 'ㄶ', 'ㄷ', 'ㄹ', 'ㄺ', 'ㄻ', 'ㄼ', 'ㄽ', 'ㄾ', 'ㄿ', 'ㅀ', 'ㅁ', 'ㅂ', 'ㅄ', 'ㅅ', 'ㅆ', 'ㅇ', 'ㅈ', 'ㅊ', 'ㅋ', 'ㅌ', 'ㅍ', 'ㅎ']
TOTAL_LIST = CHOSUNG_LIST + JUNGSUNG_LIST + JONGSUNG_LIST
korean_detector = re.compile('.*[ㄱ-ㅎㅏ-ㅣ가-힣]+.*')

def decompose(koreanSent, fmt=0):
    # fmt: format of the output. 0: list 1: string
    splittedSent = list(koreanSent)
    output = []
    for keyword in splittedSent:
        if korean_detector.match(keyword) is not None and keyword not in TOTAL_LIST:
            char_code = ord(keyword) - BASE_CODE
            char1 = int(char_code / CHOSUNG)
            output.append(CHOSUNG_LIST[char1])
            char2 = int((char_code - (CHOSUNG * char1)) / JUNGSUNG)
            output.append(JUNGSUNG_LIST[char2])
            char3 = int((char_code - (CHOSUNG * char1) - (JUNGSUNG * char2)))
            if char3 == 0:
                output.append('_')
            else:
                output.append(JONGSUNG_LIST[char3])
        else:
            output.append(keyword)
    if fmt == 0:
        return ''.join(output)
    else:
        return output

def assemble(decomposedTerm):
    if isinstance(decomposedTerm, str):
        decomposedTerm = list(decomposedTerm)
    output = []
    i = 0
    while i <= len(decomposedTerm) - 1:
        if decomposedTerm[i] in CHOSUNG_LIST:
            candidate = decomposedTerm[i:i+3]
            choIndex = CHOSUNG_LIST.index(candidate[0])
            jungIndex = JUNGSUNG_LIST.index(candidate[1])
            jongIndex = JONGSUNG_LIST.index(candidate[2])
            charVal = ((choIndex*21) + jungIndex)*28 + jongIndex + BASE_CODE
            output.append(chr(charVal))
            i += 3
        else:
            output.append(decomposedTerm[i])
            i += 1
    return ''.join(output)

if __name__ == '__main__':
    print('decomposer Assembler Test:')
    import pandas as pd
    strings = ['대한민국', 'SK이노베이션', '한글']
    dec = []
    ass = []
    for word in strings:
        dec.append(decompose(word))
        ass.append(assemble(dec[-1]))
    output = pd.DataFrame.from_dict({'Original': strings, 'Decomposer': dec, 'Assembler': ass})
    print(output)
