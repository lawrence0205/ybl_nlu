from scripts.vecEmbedding.word2vec_emb_kor import embedding
import pandas as pd
import numpy as np
import pickle
import tqdm
import os


def retrieveNode_data():
    root = os.getcwd()
    datPath = os.path.join(root, 'data')
    datPath = os.path.join(datPath, 'ontology_data')
    flist = os.listdir(datPath)
    flist = [item for item in flist if item[0].isalpha()]
    objNodes = pd.read_excel(os.path.join(datPath, flist[0]), sheet_name='object')
    intNodes = pd.read_excel(os.path.join(datPath, flist[0]), sheet_name='intent')

    objKey = objNodes['keywordBasket']
    objNodes = objNodes[~pd.isna(objKey)]
    intKey = intNodes['keyWordBasket']
    intNodes = intNodes[~pd.isna(intKey)]

    objKey = [item.split(',') for item in objNodes['keywordBasket'].tolist()]
    for idx1, item in enumerate(objKey):
        for idx2, word in enumerate(item):
            objKey[idx1][idx2] = word.strip()
    intKey = [item.split(',') for item in intNodes['keyWordBasket'].tolist()]
    for idx1, item in enumerate(intKey):
        for idx2, word in enumerate(item):
            intKey[idx1][idx2] = word.strip()

    objOutput = [objNodes['objectName'].tolist(), objKey]
    intOutput = [intNodes['intentObj'].tolist(), intKey]
    return objOutput, intOutput

def createKWDPair(keyword1: list, keyword2: list):
    output = []
    for word1 in keyword1:
        for word2 in keyword2:
            if '쿼리가 obj 그 자체' in word1:
                word1 = ''
            if '쿼리가 obj 그 자체' in word2:
                word2 = ''
            output.append([word1, word2])
    return output

def createTrainingPair(object=[], intent=[], save=''):
    if len(object) == 0 or len(intent) == 0:
        object, intent = retrieveNode_data()
    md = embedding()
    output = []
    print('Creating data')
    for idx1, kwd in enumerate(tqdm.tqdm(object[1])):
        for idx2, kwd2 in enumerate(intent[1]):
            pairs = createKWDPair(kwd, kwd2)
            pairs = [(idx1, idx2, md.wordEmb(item[0]), md.wordEmb(item[1])) for item in pairs]
            output.append(pairs)
    if len(save) > 0:
        with open(save, 'wb') as f:
            pickle.dump(output, f)
    return output

def createTrainingData(maxLine=10):
    with open('./data/text_classification_data/trainingPairs', 'rb') as f:
        dat = pickle.load(f)
    output = []
    objLabel = []
    intLabel = []
    print('Creating ndArray Data Set')
    filler = np.zeros((maxLine - 2, dat[0][0][2].shape[0])).tolist()


    splitPoints = [int(np.round(len(dat)/item)) for item in np.arange(1, 10)]
    splitCounter = 1
    for idx, comb in tqdm.tqdm(enumerate(dat)):
        for inst in comb:
            tempDat = []
            tempDat.append(inst[2].tolist())
            tempDat.append(inst[3].tolist())
            tempDat += filler
            objLabel.append(inst[0])
            intLabel.append((inst[1]))
            output.append(tempDat)
        if idx in splitPoints:
            print('Reached check point, converting to ndArray')
            output = np.array(output)
            with open('./xData' + str(splitCounter), 'wb') as f:
                pickle.dump(output, f)
            with open('./objLabel' + str(splitCounter), 'wb') as f:
                pickle.dump(objLabel, f)
            with open('./intLabel' + str(splitCounter), 'wb') as f:
                pickle.dump(intLabel, f)
            print('Saved data for split ' + str(splitCounter))
            output = []
            objLabel = []
            intLabel = []
            splitCounter += 1



if __name__ == '__main__':
    # dat = retrieveNode_data()
    createTrainingData()
