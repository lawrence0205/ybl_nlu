def addLogger(fn: str, content):
    log = readLogger(fn)
    if not isinstance(content, str):
        content = str(content)
    log.append(content)
    with open(fn, 'w') as f:
        for line in log:
            f.write(line + '\n')

def readLogger(fn: str):
    log = []
    with open(fn, 'r') as f:
        while True:
            line = f.readline()
            if not line:
                break
            log.append(line.strip())
    return log
