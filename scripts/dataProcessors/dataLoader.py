"""
The main purpose of this script is to load necessary data for the neural network training
"""
import os
import pandas as pd
from scripts.langTools.korDAssembler import *
import tqdm
import re
import sqlalchemy

def loadData(target=[]):
    root = os.path.dirname(os.getcwd())
    dataPath = os.path.join(root, 'data/fasttext_training_data')
    if 'naver' in target:
        fileDir = os.path.join(dataPath, 'naverTitleBody.txt')
        output = []
        with open(fileDir, 'r', encoding='utf8') as f:
            while True:
                line = f.readline()
                if not line:
                    break
                output.append(line.strip().split(' '))
        return output
    else:
        fileDir = os.path.join(dataPath, 'wikiTotal.txt')
        output = []
        with open(fileDir, 'r', encoding='utf8') as f:
            while True:
                line = f.readline()
                if not line:
                    break
                output.append(line.strip().split(' '))
        return output


def prepare_Naver_Data():
    #@title 데이터 업데이트를 하려면 bool_switch 에 체크하고,</br>아래 정보를 입력하세요.
    # 타겟 데이터 파일이 존재하는지 확인
    bool_switch = False  #@param {type:"boolean"}
    root = os.path.dirname(os.getcwd())
    dataPath = os.path.join(root, 'data/fasttext_training_data')
    path_data = os.path.join(dataPath, 'df_text_naver.csv')
    if not os.path.exists(path_data) or bool_switch:
        print("There is no file: {}\nDownload from database...".format(path_data))
        # 없으면 AWS RDS 에서 테이블 전체를 pandas.DataFrame으로
        # 다운로드 받는 동시에 데이터를 csv 파일로 저장한다.
        user = 'ybl'  #@param {type:"string"}
        password = 'Ybl201803'  #@param {type:"string"}
        host = 'ybl-aws-rds-1.cdf7oyzvzqno.ap-northeast-2.rds.amazonaws.com'
        port = '3306'
        db_name = 'ybl_rdb'
        charset = 'utf8'
        uri = 'mysql+pymysql://{}:{}@{}:{}/{}?charset={}'.format(
            user,
            password,
            host,
            port,
            db_name,
            charset
        )
        engine = sqlalchemy.create_engine(uri, encoding='utf-8')
        sql = 'SELECT * FROM text_naver;'
        df_data_all = pd.read_sql(sql=sql, con=engine)
        print("Data downloaded.")
        df_data_all.to_csv(path_data)
        print('Naver data file has been written.')
    else:
        # 파일이 있고 bool_switch가 False이면 pandas.DataFrame 으로 읽어들인다.
        print("File exists. Read file...")
        df_data_all = pd.read_csv(path_data)
        print("Naver data loaded.")

    path_data = os.path.join(dataPath, 'naverTitleBody.txt')
    if not os.path.exists(path_data):
        print('Preparing trainable txt file')
        toutput = []
        toutput += df_data_all['body'].tolist()
        toutput += df_data_all['title'].tolist()

        fileDir = os.path.join(dataPath, 'naverTitleBody.txt')
        with open(fileDir, 'w', encoding='utf8') as f:
            for line in tqdm.tqdm(toutput):
                if not isinstance(line, str):
                    line = str(line)
                pl = line.split('\n')
                for subline in pl:
                    subline = re.sub(r"[^[]*\[([^]]*)\]", '', subline)
                    nums = re.findall("\d+\.\d+", subline)
                    replacednums = [num.replace('.', '쏘쑤쩖') for num in nums]
                    for num, rnum in zip(nums, replacednums):
                        subline = subline.replace(num, rnum)
                    subline = subline.split('.')
                    for sentence in subline:
                        filtered = re.sub(r'\W+', ' ', sentence)
                        filtered = filtered.replace('쏘쑤쩖', '.')
                        filtered = filtered.strip()
                        if len(filtered) > 7:
                            f.write(decompose(filtered) + '\n')



if __name__ == '__main__':
    # prepare_Naver_Data()
    a = loadData('naver')
    print('done')
