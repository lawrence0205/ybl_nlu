from gensim.models import FastText
from scripts.korDAssembler import *
import numpy as np
import os

root = os.path.dirname(os.getcwd())
model = FastText.load(os.path.join(root, 'trained/korTrained.bin'))
print('Model is successfully loaded')

nbw_list = ['중국', '사과', '애플', '삼성', '문재인', '불화수소', '박근혜', '석유',
            '한국', '대한민국', '중국', '일본', '미국']
print('------------------- Neighborring Words ---------------------')
for word in nbw_list:
  nbWords = model.wv.most_similar(decompose(word))[:20]
  nbWords = [(assemble(item[0]), item[1]) for item in nbWords]
  print('Top 5 neighboring words: ' + '"' + word +'": ',
        nbWords)

print('')
fcmp_list = ['에너지', '인공지능', '관광', '금융', '부동산']
scmp_list = ['배터리', '석유', '원자력', '김치', '우동', '맛동산', '뇌',
             '이산화탄소', '알파고', '딥러닝', '강화학습', '중국', '일본',
             '아베', '채권', '주식', '사기', '전기자동차', '전기', '전자',
             '리튬', '상한가', '금리', '자동차', '광어회', '주택', '토지',
             '유전', '원전', '화폐', '금광', '테슬라', '대한항공']
print('------------------- Words Similarity ---------------------')
for fstWord in fcmp_list:
  similarityList = []
  for sndWord in scmp_list:
    similarityList.append(model.wv.similarity(decompose(fstWord), decompose(sndWord)))
  orderedIndex = np.argsort(similarityList)[::-1] # High to low
  for idx in orderedIndex[:10]:
    print(fstWord + '/' + scmp_list[idx] + ': ', similarityList[idx])
  print('')
