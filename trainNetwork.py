from scripts.dataProcessors.cnnData_Generator import *
from scripts.dataProcessors.logger import *
import scripts.textClassification.cnnModel as Nets
import tensorflow as tf
import numpy as np
import pickle
import os

saveRoot = './trained/textClassification'
# Initialization
if 'objNet1.h5' not in os.listdir(saveRoot):
    netGenerator = Nets.TextConvNet()
    model = netGenerator.objNet1(num_classes=258)
else:
    model = tf.keras.models.load_model(saveRoot + '/objNet1.h5')

# Training Session
datPath = './data/text_classification_data'
logPath = saveRoot + '/trainingLog.txt'
log = readLogger(logPath)
log = [int(item) for item in log]
for iter in np.arange(1, 9):
    if iter in log:
        continue
    with open(datPath + '/xData' + str(iter), 'rb') as f:
        xData = pickle.load(f)
        xData = np.expand_dims(xData, axis=3)
    with open(datPath + '/objLabel' + str(iter), 'rb') as f:
        objLabel = pickle.load(f)
    with open(datPath + '/intLabel' + str(iter), 'rb') as f:
        intLabel = pickle.load(f)
    model.fit(xData, objLabel, batch_size=64)
    model.save(saveRoot + '/objNet1.h5')
    log.append(iter)
    addLogger(logPath, iter)
    print('Training iteration ' + str(iter) + ' complete')


