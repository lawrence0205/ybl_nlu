from scripts.vecEmbedding.word2vec_emb_kor import embedding
from scripts.dataProcessors.cnnData_Generator import *
import tensorflow as tf
import numpy as np

md = embedding()

def query2input(query: str, maxSent=10, embDim=100):
    query = query.split(' ')
    embQuery = [md.wordEmb(item).tolist() for item in query]
    maxLen = maxSent - len(embQuery)
    if maxLen > 0:
        for i in range(maxLen):
            embQuery.append([0]*100)
    embQuery = np.array(embQuery)
    embQuery = np.expand_dims(embQuery, axis=0)
    embQuery = np.expand_dims(embQuery, axis=3)
    return embQuery

objNodes, intNodes = retrieveNode_data()
objNodes = objNodes[0]
intNodes = intNodes[0]

Query = '운송인프라 뉴스'
Query = query2input(Query)
saveRoot = './trained/textClassification'
model = tf.keras.models.load_model(saveRoot + '/objNet1.h5')
predicted = model.predict(Query)

print(objNodes[np.argmax(predicted[0])])

